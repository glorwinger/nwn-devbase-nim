import shared

let args = DOC """
Neverwinter Nights Module Tools

Usage:
    nwn_module [options] (pack | unpack <module> | compile | clean | resman)
    $USAGE

Options:
    unpack module               Unpack the module file
    pack                        Pack the module file
    compile                     Compile scripts
    resman                      Create resman symlinks
    clean                       Clean the project folders
    -p, --project-dir DIR       Project folder [default: .]
    -n, --nwn-dir nDIR          NWN Install dir
    $OPT
"""

var prjDir = ""
if $args["--project-dir"] == ".":
    prjDir = getCurrentDir()
elif isAbsolute($args["--project-dir"]):
    prjDir = $args["--project-dir"]
else:
    echo "Project Folder must be an absolute path"
    quit(QuitFailure)

var nwnDir = ""
if args["pack"] or args["compile"]:
    if isAbsolute($args["--nwn-dir"]):
        nwnDir = $args["--nwn-dir"]
    else:
        echo "NWN install Folder must be an absolute path"
        quit(QuitFailure)

let project_root = prjDir
let nwn_root = nwnDir
let start_time = getTime()
let cache_dir = project_root & "/cache"
let source_dir = project_root & "/source"
let tmp_cache_dir = cache_dir & "/tmp"
let gff_cache_dir = cache_dir & "/gff"
let nss_dir = project_root & source_dir & "/nss"
let all_nss = nss_dir & "/*.nss"

proc clean =
    try:
        echo "Cleaning " & cache_dir
        removeDir(cache_dir)
    except OSError:
        echo "Could not remove folder"
        quit(QuitFailure)
    except:
        echo "Unknown error removing folder"
        quit(QuitFailure)


proc initDirectories =
    var res = false
    try:
        echo "Creating " & tmp_cache_dir
        createDir(tmp_cache_dir)
        echo "Creating " & gff_cache_dir
        createDir(gff_cache_dir)
        echo "Creating " & source_dir
        createDir(source_dir)
    except OSError:
        echo "Could not create folder"
        quit(QuitFailure)
    except:
        echo "Unknown error creating folder"
        quit(QuitFailure)


proc nssCompile () =
    echo "  Started compiling sripts "
    try:
        setCurrentDir(source_dir)
    except OSError:
        echo "Cannot change directory to " & source_dir
        quit(QuitFailure)
    except:
        echo "Unknown exception change directory " & source_dir
        quit(QuitFailure)
    
    createDir("ncs")
    createDir("ndb")

    setCurrentDir (joinPath(source_dir,"nss"))
    var cmpResult = execShellCmd("nwnsc -q -o -y -n " & nwn_root & " -b " & gff_cache_dir & " *.nss")
    if cmpResult == 0:
        echo "  Finished compiling scripts "
    else: 
        echo "ERROR compiling scripts"
        quit(QuitFailure)

        

proc extractModule (modulename: string) =
    var modulefile = joinPath(project_root, modulename)
    if not existsFile (modulefile):
        echo "Module file " & modulename & " not found in folder " & project_root
        quit(QuitFailure)
    
    var moduleTime = getLastModificationTime(modulefile)
    var dirtyFiles = false
    for file in walkFiles(source_dir & "*.*"):
        if getLastModificationTime(file) > moduleTime:
            dirtyFiles = true
            break
    
    if dirtyFiles:
        echo "An upacked module exists.  Run clean or pack before upacking"
        quit(QuitFailure)
    
    initDirectories()
    echo "Started extracting module " & modulename
    try:
        setCurrentDir(tmp_cache_dir)
    except OSError:
        echo "Cannot change directory to " & tmp_cache_dir
        quit(QuitFailure)
    except:
        echo "Unknown exception change directory " & tmp_cache_dir
        quit(QuitFailure)
        
    echo "  Started unpacking module " & modulename
    var extResult = execShellCmd("nwn_erf -x -f " & modulefile)
    if extResult == 0:
        echo "  Finished unpacking module " & modulename
    else: 
        echo "ERROR unpacking module " & modulename
        quit(QuitFailure)

    echo "  Started converting module assets"
    try:
        setCurrentDir(tmp_cache_dir)
    except OSError:
        echo "Cannot change directory to " & tmp_cache_dir
        quit(QuitFailure)
    except:
        echo "Unknown exception change directory " & tmp_cache_dir
        quit(QuitFailure)

    var cnvResult = 0
    for ext in GffExtensions.withProgressBar:
        createDir(joinPath(source_dir, ext))
        for file in walkFiles(joinPath("*." & ext)):
            # cnvResult = execShellCmd("nwn-gff -i " & joinPath(tmp_cache_dir, file) & " -o " & joinPath(source_dir, ext, file & ".yml"))
            cnvResult = execShellCmd("nwn_gff -p -i " & joinPath(tmp_cache_dir, file) & " -o " & joinPath(source_dir, ext, file & ".json"))
            if cnvResult != 0:
                echo "Conversion failure " & file
                quit(QuitFailure)
    createDir(joinPath(source_dir,"nss"))
    for nssFile in walkFiles("*.nss"):
        copyFileWithPermissions(nssFile, joinPath(source_dir, "nss", nssFile))
    echo "  Finished converting module assets"
    echo "Finished extracting module " & modulename

proc packModule () = 
    initDirectories()

    var moduleJson = parseFile(joinPath(source_dir,"ifo","module.ifo.json"))
    var modulename = getStr(moduleJson["Mod_Name"]["value"]["0"])

    echo "Started generating module " & modulename
    var cnvResult = 0
    var ext = ""
    for dir in walkDirs(joinPath(source_dir,"*")):
        setCurrentDir(dir)
        var (path, ext) = splitPath(dir)
        if ext == "nss":
            for nssFile in walkFiles(joinPath("*.nss")):
                copyFileWithPermissions(nssFile, joinPath(tmp_cache_dir, nssFile))
        else:
            for file in walkFiles("*.*"):
                var (directory, name, extenstion) = splitFile(file)
                # cnvResult = execShellCmd("nwn-gff -i " & file & " -o " & joinPath(tmp_cache_dir, name))
                cnvResult = execShellCmd("nwn_gff -i " & file & " -o " & joinPath(tmp_cache_dir, name))
                if cnvResult != 0:
                    echo "Conversion failure " & file
                    quit(QuitFailure)            
    
    nssCompile()
    
    try:
        setCurrentDir(tmp_cache_dir)
    except OSError:
        echo "Cannot change directory to " & tmp_cache_dir
        quit(QuitFailure)
    except:
        echo "Unknown exception change directory " & tmp_cache_dir
        quit(QuitFailure)

    echo "  Started packing module " & modulename

    var extResult = execShellCmd("nwn_erf -c . -e mod -f " & joinPath (project_root, modulename & ".mod"))
    if extResult == 0:
        echo "  Finished packing module " & modulename
    else: 
        echo "ERROR packing module " & modulename
        quit(QuitFailure)

    echo "Finished generating module " & modulename
   



if args["unpack"]:
    let modulename = $args["<module>"]
    echo "unpack ", modulename
    extractModule(modulename)

if args["pack"]:
    echo "pack"
    packModule()

if args["compile"]:
    echo "compile"
    nssCompile()

if args["resman"]:
    echo "resman"

if args["clean"]:
    echo "clean"
    clean()

    