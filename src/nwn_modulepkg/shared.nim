

import strutils, os, logging, version, times, rdstdin, progressbar, json
export strutils, os, logging, version, times, rdstdin, progressbar, json

import docopt as docopt_internal
export docopt_internal

const GffExtensions = @[
  "utc", "utd", "ute", "uti", "utm", "utp", "uts", "utt", "utw",
  "git", "are", "gic", "ifo", "fac", "ssf", "dlg", "itp",
  "bic", "jrl", "gff", "gui"
]
export GffExtensions

when defined(profiler):
    import nimprof
  
addHandler newFileLogger(stderr, fmtStr = "$levelid [$datetime] ")
  
const GlobalUsage = """
    $0 -h | --help
    $0 --version
"""

# Options common to ALL utilities
let GlobalOpts = """

Logging:
    --verbose                   Turn on debug logging
    --quiet                     Turn off all logging except errors
    --version                   Show program version and licence info
"""

var Args: Table[string, docopt_internal.Value]

proc DOC*(body: string): Table[string, docopt_internal.Value] =
    let body2 = body.replace("$USAGE", GlobalUsage).
                    replace("$0", getAppFilename().extractFilename()).
                    replace("$OPT", GlobalOpts)

    result = docopt_internal.docopt(body2)
    Args = result

    if Args["--version"]:
        printVersion()
        quit()

    if Args.hasKey("--verbose") and Args["--verbose"]: setLogFilter(lvlDebug)
    elif Args.hasKey("--quiet") and Args["--quiet"]: setLogFilter(lvlError)
    else: setLogFilter(lvlInfo)

