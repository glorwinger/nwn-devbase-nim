# Package

version       = "0.1.0"
author        = "Kevin Sheehan"
description   = "Neverwinter Nights Module Tools"
license       = "MIT"

srcDir        = "src"
binDir        = "bin"
bin           = @["nwn_module"]

# Dependencies

requires "nim >= 0.10.0"
requires "docopt >= 0.1.0"
